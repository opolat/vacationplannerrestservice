package de.akquinet.vacationplanner.rest;

import javax.ws.rs.core.UriBuilder;

import org.junit.BeforeClass;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * REST abstract test class provides the general web resource.
 */
public abstract class AbstractRestTest {
	private static WebResource service;

	/**
	 * Gets invoked before all JUnit tests.
	 * 
	 * @throws Exception
	 *             is thrown e.g. if the URL is invalid
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		service = client.resource(UriBuilder.fromUri(
				"http://localhost:8080/VacationPlannerRestService").build());
	}

	/**
	 * Returns the web resource related to the REST service.
	 * 
	 * @return web resource
	 */
	protected WebResource getService() {
		return service;
	}
}
