package de.akquinet.vacationplanner.rest;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MediaType;

import org.junit.Test;

/**
 * Tests the employees REST service.
 */
public class EmployeesResourceTest extends AbstractRestTest {

	/**
	 * Tests the retrieval of an employee list.
	 */
	@Test
	public void testGet() {
		String response = getService().path("employees")
				.accept(MediaType.TEXT_XML).get(String.class);

		assertTrue("response is empty", !response.isEmpty());
		assertTrue("response doesn't contain '<givenName>Max</givenName>': "
				+ response, response.contains("<givenName>Max</givenName>"));
	}
}
