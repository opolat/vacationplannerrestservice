package de.akquinet.vacationplanner.rest;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.MediaType;

import org.junit.Test;

import de.akquinet.vacationplanner.dtos.Employee;

/**
 * Tests the employee REST service.
 */
public class EmployeeResourceTest extends AbstractRestTest {

	/**
	 * Tests the retrieval of an employee.
	 */
	@Test
	public void testGetEmployee1() {
		String response = getService().path("employees").path("1")
				.accept(MediaType.TEXT_XML).get(String.class);

		assertTrue("response is empty", !response.isEmpty());
		assertTrue("response doesn't contain '<givenName>Max</givenName>': "
				+ response, response.contains("<givenName>Max</givenName>"));
	}

	/**
	 * Tests the retrieval of an employee.
	 */
	@Test
	public void testGetEmployee2() {
		String response = getService().path("employees").path("2")
				.accept(MediaType.TEXT_XML).get(String.class);

		assertTrue("response is empty", !response.isEmpty());
		assertTrue("response doesn't contain '<givenName>Petra</givenName>': "
				+ response, response.contains("<givenName>Petra</givenName>"));
	}

	/**
	 * Tests the creation and deletion of an employee.
	 */
	@Test
	public void testAddAndDeleteEmployee() {
		Employee employee = new Employee();
		employee.setGivenName("Franz");
		employee.setGivenName("Meier");
		employee.setId("3");
		getService().path("employees").put(employee);
		getService().path("employees").path("3").delete();
	}

	/**
	 * Tests the creation, modification and deletion of an employee.
	 */
	@Test
	public void testAddModifyAndDeleteEmployee() {
		Employee employee = new Employee();
		employee.setGivenName("Klaus");
		employee.setLastName("Meier");
		employee.setId("4");
		getService().path("employees").put(employee);

		Employee employeeModified = new Employee();
		employeeModified.setGivenName("Klaus");
		employeeModified.setLastName("Meier-Meister");
		employeeModified.setId("4");
		getService().path("employees").put(employeeModified);

		String response = getService().path("employees").path("4")
				.accept(MediaType.TEXT_XML).get(String.class);

		assertTrue("response is empty", !response.isEmpty());
		assertTrue(
				"response doesn't contain '<lastName>Meier-Meister</lastName>': "
						+ response,
				response.contains("<lastName>Meier-Meister</lastName>"));

		getService().path("employees").path("4").delete();
	}

}
