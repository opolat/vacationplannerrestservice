package de.akquinet.vacationplanner.rest;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 * Rest service for CRUD operation regarding employee vacations.
 */
public class VacationsResource {
	private String employeeId;

	/**
	 * Initializes the resource with the unique identifier of the employee.
	 * 
	 * @param employeeId
	 *            employee ID
	 */
	public VacationsResource(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Creates the sub URL path with the given vacation ID.
	 */
	@Path("{id}")
	public VacationResource getVacation(@PathParam("id") String id) {
		return new VacationResource(employeeId, id);
	}
}
