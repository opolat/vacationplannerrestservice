package de.akquinet.vacationplanner.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import de.akquinet.vacationplaner.servicemock.EmployeeServiceMock;
import de.akquinet.vacationplanner.dtos.Employee;

/**
 * Rest service for CRUD operation regarding employees.
 */
@Path("/employees")
public class EmployeesResource {

	/**
	 * Returns a list of all available employees.
	 * <p />
	 * Invoke this URL to see the GET result:
	 * http://localhost:8080/VacationPlannerRestService/employees/
	 */
	@GET
	@Produces({ MediaType.TEXT_XML })
	public List<Employee> getEmployees() {
		return EmployeeServiceMock.getEmployees();
	}

	/**
	 * Creates or updates an employee.
	 * <p />
	 * Invoke this URL with PUT:
	 * http://localhost:8080/VacationPlannerRestService/employees/
	 * 
	 * @param employee
	 *            employee
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public void putEmployee(JAXBElement<Employee> employee) {
		EmployeeServiceMock.addOrUpdateEmployee(employee.getValue());
	}

	/**
	 * Creates the sub URL path with the given employee ID.
	 */
	@Path("{id}")
	public EmployeeResource getEmployee(@PathParam("id") String id) {
		return new EmployeeResource(id);
	}
}
