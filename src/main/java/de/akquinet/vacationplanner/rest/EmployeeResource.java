package de.akquinet.vacationplanner.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import de.akquinet.vacationplaner.servicemock.EmployeeServiceMock;
import de.akquinet.vacationplanner.dtos.Employee;
import de.akquinet.vacationplanner.dtos.Vacation;

/**
 * Rest service for CRUD operation regarding a specific employee.
 */
public class EmployeeResource {
	private String employeeId;

	/**
	 * Initializes the resource with the unique identifier of the employee.
	 * 
	 * @param employeeId
	 *            employee ID
	 */
	public EmployeeResource(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * Returns the data of an employee including his vacations.
	 * <p />
	 * Invoke this URL to see the GET result (e.g. employee with the ID 1):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/
	 * 
	 * @return employee
	 */
	@GET
	@Produces({ MediaType.TEXT_XML })
	public Employee getEmployee() {
		return EmployeeServiceMock.getEmployee(employeeId);
	}

	/**
	 * Creates or updates a vacation.
	 * <p />
	 * Invoke this URL with PUT (e.g. the employee with the ID 1):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/
	 * 
	 * @param vacation
	 *            vacation
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_XML)
	public void putVacation(JAXBElement<Vacation> vacation) {
		Employee employee = EmployeeServiceMock.getEmployee(employeeId);

		if (employee != null) {
			EmployeeServiceMock.addOrUpdateVacation(vacation.getValue(),
					employee);
		}
	}

	/**
	 * Deletes the employee.
	 * <p />
	 * Invoke this URL with DELETE (e.g. the employee with the ID 1):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/
	 */
	@DELETE
	public void deleteEmployee() {
		EmployeeServiceMock.deleteEmployee(employeeId);
	}

	/**
	 * Creates the sub URL path with the given employee ID.
	 */
	@Path("/vacations")
	public VacationsResource getVacations() {
		return new VacationsResource(employeeId);
	}
}
