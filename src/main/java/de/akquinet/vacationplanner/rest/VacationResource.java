package de.akquinet.vacationplanner.rest;

import javax.ws.rs.DELETE;

import de.akquinet.vacationplaner.servicemock.EmployeeServiceMock;
import de.akquinet.vacationplanner.dtos.Employee;

/**
 * Rest service for CRUD operation regarding a specific vacation.
 */
public class VacationResource {
	private String employeeId;
	private String vacationId;

	/**
	 * Initializes the resource with the unique identifiers.
	 * 
	 * @param employeeId
	 *            employee ID
	 * @param vacationId
	 *            vacation ID
	 */
	public VacationResource(String employeeId, String vacationId) {
		this.employeeId = employeeId;
		this.vacationId = vacationId;
	}

	/**
	 * Deletes the vacation.
	 * <p />
	 * Invoke this URL with DELETE (e.g. the employee with the ID 1 and vacation
	 * ID 104):
	 * http://localhost:8080/VacationPlannerRestService/employees/1/vacations
	 * /104
	 */
	@DELETE
	public void deleteVacation() {
		Employee employee = EmployeeServiceMock.getEmployee(employeeId);

		if (employee != null) {
			EmployeeServiceMock.deleteVacation(vacationId, employee);
		}
	}
}
