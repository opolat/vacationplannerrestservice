

# Installation #

* Install JDK 8
  http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
* Install Eclipse IDE
  https://eclipse.org/downloads/eclipse-packages/
* Install Tomcat 8
  download & unzip: http://mirror.netcologne.de/apache.org/tomcat/tomcat-8/v8.0.36/bin/apache-tomcat-8.0.36.zip
* Install Git-Client (optional)
  https://git-scm.com/downloads


# Checkout Project with Git-Client #
git clone https://bitbucket.org/opolat/vacationplannerrestservice.git

# Checkout Project with Eclipse #
* open eclipse
* goto Project-Explorer
* right mouse-click "import/Git/Projects from Git"




